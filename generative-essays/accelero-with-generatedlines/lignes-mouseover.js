//// 0. Describe this demo
window.demoDescription = "In a field of points that revolves around a center, trace a perpendicular path from each point to an axis line.";


Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function hex2rgb( hex ) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
        alpha: 1
    } : null;
}

Math.degrees = function(radians) {
  return radians * 180 / Math.PI;
};

function animateFilling (point, animDuration) {
  TweenMax.fromTo(point._percent, animDuration, {value: 0}, {value: 1, onComplete: function () {
    this._fullBarColor = this._lineColor;
    this._lineColor = {r: '255', g: '255', b: '255', alpha: 1};
    TweenMax.to(this._lineColor, 0.2, {alpha: 0})
  }.bind(point)})
}


//// 1. Define Space and Form
var colors = {
  a1: "#35dab8", a2: "#e74c3c", a3: "#9b59b6", a4: "#f1c40f",
  a5: "#e8a32d", a6: "#fe7139", a7: "#fd7a99", a8: "#053a7c"
};
var colorNumber = 8;
var space = new CanvasSpace("demo", "#222" ).display();
var form = new Form( space );
var linesColored = false;
var isAnimating = false;

var animDuration = 3;
var intervalDuration = animDuration*1000 + 300;

//// 2. Create Elements
var pts = [];
var center = space.size.$divide(2);
var line = new Line(center).to( space.size );


form.fill('#FFFFFF').font(50).text(new Point( 0, 0), 'Stay');


var count = 200;
var r = Math.min( space.size.x, space.size.y ) * 0.8;
for (var i=0; i<count; i++) {
  var p = new Vector( Math.random()*r-Math.random()*r, Math.random()*r-Math.random()*r );
  p.moveBy( center ).rotate2D( i*Math.PI/count, center );
  p._color = '';
  p._fullBarColor = {r: '255', g: '255', b: '255', alpha: 0.8};
  p._lineColor = {r: '255', g: '255', b: '255', alpha: 0};
  p._progressPointCoordinates = {x: p.x, y: p.y, z: p.z};
  p._percent = {value: 0};
  pts.push( p );
}

//// 3. Visualize, Animate, Interact
space.add({
  animate: function(time, fps, context) {
    for (var i=0; i<pts.length; i++) {

      // rotate the points slowly
      var pt = pts[i];
      pt._color = colors["a" + ((i % colorNumber) + 1)];
      pt.rotate2D( Const.one_degree / 20, center );
      form.stroke( false ).fill( pt._color ).point( pt, 1 );

      // get line from pt to the mouse line
      var perpendicularPoint = line.getPerpendicularFromPoint( pt );
      var ln = new Line( pt ).to( perpendicularPoint );
      var colorTarget = {x: ((perpendicularPoint.x - pt.x) * pt._percent.value) + pt.x, y: ((perpendicularPoint.y - pt.y) * pt._percent.value) +pt.y, z: 0};
      var lnColor = new Line( pt ).to(colorTarget);

      // opacity of line derived from distance to the line
      var opacity = Math.min( 0.8, 1 - Math.abs( line.getDistanceFromPoint( pt ) ) / r );

      form.stroke( "rgba("+ pt._fullBarColor.r +","+ pt._fullBarColor.g +","+ pt._fullBarColor.b +", "+ pt._fullBarColor.alpha +")", 2*(i%20)/20 ).fill( false ).line( ln );
      form.stroke( "rgba("+ pt._lineColor.r +","+ pt._lineColor.g +","+ pt._lineColor.b +", "+ pt._lineColor.alpha +")", 2*(i%20)/20 ).fill( false ).line( lnColor );
    }
  },
  onMouseAction: function(type, x, y, evt) {
    if (type=="move") {
      // line.p1.set( x, y );
    }
  }
});


// 4. Start playing
space.bindMouse();
space.play();

// 5. Add events
var hoverInterval = false;

function fillLines () {
  for (var i=0; i<pts.length; i++) {
    if (!linesColored) {
      var targetCoordinate = line.getPerpendicularFromPoint( pts[i] );
      animateFilling(pts[i], animDuration);
      pts[i]._lineColor = hex2rgb(pts[i]._color) || {r: '255', g: '255', b: '255', alpha: '1'};
    } else {
      animateFilling(pts[i], animDuration);
      pts[i]._lineColor = {r: '255', g: '255', b: '255', alpha: '1'};
    }
  }
  linesColored = !linesColored;
}

// Events

$('#stay-updated').on('mouseenter', function (e) {
  if(hoverInterval) {
    return;
  }
  fillLines();
  hoverInterval = setInterval(fillLines, intervalDuration)
});
$('#stay-updated').on('mouseleave', function (e) {
  clearInterval(hoverInterval);
  hoverInterval = false;
});

$('#pt').on('mousemove', function (e) {
  line.p1.set( e.clientX, e.clientY );
});
$('.close-btn').on('click', function (e) {
  $('.popup').fadeOut('400');
});