

from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
#from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket

clients = []
class SimpleChat(WebSocket):

    def handleMessage(self):
       for client in clients:
          if client != self:
             client.sendMessage(self.data)
             #client.sendMessage(self.address[0] + u' - ' + self.data)

    def handleConnected(self):
       print self.address, 'connected'
       for client in clients:
          client.sendMessage(self.address[0] + u' - connected')
       clients.append(self)

    def handleClose(self):
       clients.remove(self)
       print self.address, 'closed'
       for client in clients:
          client.sendMessage(self.address[0] + u' - disconnected')

server = SimpleWebSocketServer('', 9999, SimpleChat)
server.serveforever()

class SimpleEcho(WebSocket):

    def handleMessage(self):
        # echo message back to client
        self.sendMessage(self.data)

    def handleConnected(self):
        print self.address, 'connected'

    def handleClose(self):
        print self.address, 'closed'

#server = SimpleWebSocketServer('', 9999, SimpleEcho)
#server.serveforever()

class StepCatcher(WebSocket):

	""" Add walker to their path """
	def handleConnect(self):
		pass

	""" Remove walker from their path """
	def handleClose(self):
		pass
	

