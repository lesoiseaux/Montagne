$(window).scroll(function(e){
  parallax();
});
function parallax(){
  var scrolled = $(window).scrollTop();
  $('#bg1').css('left',-(scrolled*2)+'px');
  $('#bg2').css('right',-(scrolled*2)+'px');
  $('#image1l').css('bottom',-(scrolled*0.2)+'px');
  $('#image1r').css('bottom',-(scrolled*0.2)+'px');
  $('#image2r').css('left',-(scrolled*0.02)+'px');
  $('#image3r').css('bottom',-(scrolled*0.1)+'px');
  $('#image4r').css('left',-(scrolled*0.01)+'px');
  $('#image1l').css('bottom',-(scrolled*0.2)+'px');
  $('#image2l').css('right',-(scrolled*0.02)+'px');
  $('#image3l').css('bottom',-(scrolled*0.1)+'px');
  $('#image4l').css('right',-(scrolled*0.01)+'px');
}
