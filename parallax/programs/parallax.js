$(window).scroll(function(e){
  parallax();
});
function parallax(){
  var scrolled = $(window).scrollTop();
  $('#image1r').css('right',-(scrolled*0.2)+'px');
  $('#image2r').css('left',-(scrolled*0.2)+'px');
  $('#image3r').css('bottom',-(scrolled*0.1)+'px');
  $('#image4r').css('left',-(scrolled*0.1)+'px');
  $('#image1l').css('bottom',-(scrolled*0.2)+'px');
  $('#image2l').css('left',-(scrolled*0.2)+'px');
  $('#image3l').css('bottom',-(scrolled*0.1)+'px');
  $('#image4').css('left',-(scrolled*0.1)+'px');
}
