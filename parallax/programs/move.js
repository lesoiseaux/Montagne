var n = 100
var i

function rand() {
    return (
      Math.random() + 
      Math.random() + 
      Math.random() + 
      Math.random() + 
      Math.random() + 
      Math.random()) / 3 - 1
}

var p = document.querySelector('polyline')

var rot = -Math.PI / 12

var X = [], Y = [], Z = []
for(i = 0; i < n; i++) {
  X.push(800 * rand())
  Y.push(500 * rand())
  Z.push(1000 * rand())
}

var points = []
for(i = 0; i < n; i++) {
  points.push([
    480 + X[i] * Math.cos(rot) - Y[i] * Math.sin(rot), 
    240 + Y[i] * Math.cos(rot) + X[i] * Math.sin(rot) 
  ])
}

// first we have to populate `points` via an attrib string
// because one can't unfortunately create an SVGPoint


var divs = []
for(i = 0; i < n; i++) {
  divs.push(document.body.appendChild(document.createElement("DIV")))
}

window.requestAnimationFrame(function render() {
    
  var div
  rot = performance.now() / 2000
  var cos = Math.cos(rot)
  var sin = Math.sin(rot)
  for(i = 0; i < n; i++) {
    div = divs[i]
    div.style.transform = 'translate3d(' +  points[i][1] + 'px, ' + (480 + X[i] * cos - Z[i] * sin) + 'px, 0px)'
  }
 window.requestAnimationFrame(render)
})  

