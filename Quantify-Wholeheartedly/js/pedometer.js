$(document).ready(function(){

console.log("print");

//on déclare l'ensemble des variables ax et ay sont les données d'acceleration, n c'est la norme, maxn et minn les valeurs max et min de la norme, et pas le nombre de pas
var n=0, maxn = 0, minn = 0, pas = 0,
	ax = 0, ay = 0, az = 0, pass=0;

//programme	d'alberto desarullo qui va chercher les données de l'accelerometre
if (window.DeviceMotionEvent != undefined) {
	window.ondevicemotion = function(e) {
		ax = event.accelerationIncludingGravity.x ;
		ay = event.accelerationIncludingGravity.y ;
		az = event.accelerationIncludingGravity.z ;
// et les affiche dans la page html
		document.getElementById("accelerationX").innerHTML = e.accelerationIncludingGravity.x;
		document.getElementById("accelerationY").innerHTML = e.accelerationIncludingGravity.y;
		document.getElementById("accelerationZ").innerHTML = e.accelerationIncludingGravity.z;
// on affiche également la norme sa valeur max et le nombre de pas
		document.getElementById("n").innerHTML = n;
		document.getElementById("pas").innerHTML = pas;
		document.getElementById("maxn").innerHTML = maxn;

		}		
	

    var ev = new Event("requestMove");

	function pollAccelerometer() {
		setInterval( function() {
	
	// on calcule la norme c'est  à dire la taille du vecteur qui est défini par les points d'accélération ax ay et az
			n0 = n;
			n=Math.sqrt (ax*ax + ay*ay + az*az);
	//		console.log(n);

	//on calcule un min et max pour la norme		
			if (n < n0) { minn = n ;
			}else if (n > n0) { maxn = n ;
			}
	
	//		console.log(maxn);

	//on défini un seuil qui correspond à un pas
			if ((maxn - minn) > 10) {pas = pas+1;
            
			}


			if (pas>10) {
	//			console.log(pas);
				document.dispatchEvent(ev);
			}
			pas++;		
			// on vérifie cela chaque 250 milisecondes			 	
		}, 350);
    }

//function mockAccelerometer() {
//	setInterval( function() {
//		console.log(pas);
//		if (pas===100) {
//				document.dispatchEvent(ev);
//		}
//		pas++;
//	},1000);		
//}

	
pollAccelerometer();
} 
});

