//points from SVG path run through Snap. BAM
var data = Snap.path.toCubic(document.getElementById("path").getAttribute('d'))
    dataLength = data.length,
    points = [], //holds our series of x/y values for anchors and control points,
    pointsString = data.toString();


// convert cubic data to GSAP bezier
for (var i = 0; i < dataLength; i++) {
  var seg = data[i];
  if (seg[0] === "M") { // move (starts the path)
    var point = {};
    point.x = seg[1];
    point.y = seg[2];
    points.push(point);
  } 
  else { // seg[0] === "C" (Snap.path.toCubic should return only curves after first point)
    for (var j = 1; j < 6; j+=2) {
      var point = {};
      point.x = seg[j];
      point.y = seg[j+1];
      points.push(point);
    }
  }
}

//make the tween


let cb=function(e) { console.log("ok"); TweenLite.set("#wingr", {x:points[0].x, y:points[0].y})

var tween = TweenMax.to("#wingr", 8, {bezier:{type:"cubic", values:points}, force3D:true, ease:Power0.easeNone});
  
}
	

Snap.animate(0, flight_path_length, function( step ) {
                moveToPoint = Snap.path.getPointAtLength( flight_path, step );
                x = moveToPoint.x - (spaceshipbbox.width/2);
                y = moveToPoint.y - (spaceshipbbox.height/2);
                spaceship.transform('translate(' + x + ',' + y + ') rotate('+ (moveToPoint.alpha - 90)+', '+spaceshipbbox.cx+', '+spaceshipbbox.cy+')');
            },5000, mina.easeout ,function(){
                ship_move_up();
            });

document.addEventListener("requestMove", cb,false)

