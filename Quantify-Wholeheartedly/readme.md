## Quantify Wholeheartedly

Voici les premiers essais d'assemblages graphiques, sous la forme d'un jeu, une sorte de puzzle qui fait évoluer des morceaux de dessins au fur et à mesure des données de pas envoyées par un accéléromètre, pour finalement recomposer un organe du corps humain.Les images originales sont des images médicales du début du XXe disponibles ici: https://commons.wikimedia.org/wiki/File:The_diagnosis_of_diseases_of_women_%281905%29_%2814576843969%29.jpg J'ai vectorisé l'image puis j'en ai détaché différents morceaux manuellement en en redessinant les contours dans Inkscape, les fichiers sont dans le dossier svg.

Cette application manipule les des fichiers svg grace à la librairies snap-svg.
Elle les animes en suivant un path prédéterminé nommé '#path' dans le fichier svg.
La longueur du déplacement est déterminé par celui ci dans la variable fly-path.
Les étapes de l'avancée sont déterminées par la variable 'step'.
Chaque étape est déclenchée par les pas mesurés par l'accéléromètre.

